# Prerequisist

Run `yarn install`

## Start API

To start the API run `yarn start` this will start the API on port 3000. It is also possible to define the port by setting the NodeJS environment variable `process.env.PORT`

## Test

Run `yarn test` to execute the tests
