import { handler } from "./intersectionNameHandler";

test("return same point as given", () => {
  const expectedResult = {
    CNN: 24532000,
    Active: 1,
    X: 6013809.344,
    Y: 2115135.649,
    GISObjectID: 303675,
    Latitude: 37.78853592,
    Longitude: -122.3960348,
    DateInsert: "00:00.0",
    DateUpdate: "00:00.0",
    streetname: "01ST ST",
    cross_street: "HOWARD ST"
  };

  const result = handler(expectedResult.Latitude, expectedResult.Longitude);

  expect(result.streetName).toBe(expectedResult.streetname);
});

test("Return Stevenson when given '37.791088, -122.399137'", () => {
  const expectedResult = {
    CNN: 24666000,
    Active: 1,
    X: 6013086.288,
    Y: 2115891.559,
    GISObjectID: 303770,
    Latitude: 37.79057121,
    Longitude: -122.3985895,
    DateInsert: "00:00.0",
    DateUpdate: "00:00.0",
    streetname: "01ST ST",
    cross_street: "STEVENSON ST"
  };

  const result = handler(37.791088, -122.399137);

  expect(result.streetName).toBe(expectedResult.streetname);
});
