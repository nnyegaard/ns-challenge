import Router from "koa-router";
import { handler } from "./intersectionNameHandler";

export const intersectionName = new Router();

intersectionName.get("/", async ctx => {
  const { latitude, longitude }: { latitude: number; longitude: number } = ctx.query;

  if (isNaN(latitude) || isNaN(longitude)) {
    ctx.status = 401;
    ctx.body = {
      status: "failed",
      message: `latitude or longitude was not given correctly. Need to be a number. Input received was: Latitude: ${latitude}, Longitude: ${longitude}`
    };
  } else {
    const result = handler(latitude, longitude);
    ctx.status = 200;
    ctx.body = {
      status: "success",
      data: JSON.stringify(result)
    };
  }
});
