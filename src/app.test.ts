import { server } from "./app";
import request = require("supertest");

afterAll(() => {
  server.close();
});

test("get 401 when missing latitude and longitude", async () => {
  const response = await request(server).get("/");
  expect(response.status).toEqual(401);
});

test("get 401 when missing latitude", async () => {
  const response = await request(server).get("/?longitude=321");
  expect(response.status).toEqual(401);
});

test("get 401 when missing longitude", async () => {
  const response = await request(server).get("/?latitude=123");
  expect(response.status).toEqual(401);
});

test("get 200 when providing latitude and longitude", async () => {
  const response = await request(server).get("/?latitude=123&longitude=321");
  expect(response.status).toEqual(200);
});
