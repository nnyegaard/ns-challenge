import Koa from "koa";
import Logger from "koa-logger";
import { intersectionName } from "./features/intersectionName/intersectionNameController";

const app = new Koa();
const PORT = process.env.PORT || 3000;

app.use(async (ctx, next) => {
  try {
    await next();
  } catch (error) {
    ctx.status = error.status || 500;
    ctx.body = error.message;
    ctx.app.emit("error", error, ctx);
  }
});

app.use(Logger());
app.use(intersectionName.routes());

export const server = app.listen(PORT, () => {
  console.log(`Server listening on port: ${PORT}`);
});
